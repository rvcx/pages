# My thoughts on the Best Picture nominees for the 91st Academy Awards

I didn't think there were any truly great films among this year's crop of nominees. Here's what I thought of them, from worst to best:

## Bohemian Rhapsody

It's embarrassing that this was even nominated. It's the first biopic I've ever sat through that left me feeling the subject was actually less nuanced and interesting than when I went in. Oh Mercury was desperate for validation? Success went to his head? He got sucked into a world of sex and drugs and lost touch with the music and his real friends? The character in this film is literally nothing but a string of Behind The Music clichés.

But beyond that, it's just poorly made. There's no sense of time. They were just a bunch of guys who sold their van for studio time, and then they're signed, and then I guess they're the biggest band in the world and can fire their producer? The actual rise goes completely unnoticed. And then they break up...but it's not clear how successful they were at the time, or for how long they broke up, or what anyone was doing in the meantime.

The line that really drove me crazy is when somebody (I don't even remember who) goes to find Freddie with his druggie pals and says "they don't care about you", after we'd seen nothing but a transactional relationship between him and his band-mates as well. There's an interesting question here about whether it's worth thinking of anyone as "family", and whether it matters less whether we're using each other than whether we like who we get to be when we give them what they want. But that's not what the film was about.

All that said, QUEEN'S MUSIC IS AMAZING! This movie is a terrible exemplar of filmmaking...but still a great time.

The whole experience is really best summed up by the realization that I enjoyed watching the closing credits (Don't Stop Me Now, with real concert footage) at least as much as any actual scene in the film.


## Vice

I hadn't realized that this was made by the same guy who did The Big Short (which I loathed), but within half an hour I was thinking "this is as bad as The Big Short". And it got worse after that.

The opening tries to make the excuse that Cheney is very secretive. But you know what? If all you know about someone are the scantest biographical details, don't have any particular attitude towards them, and don't have any larger message, then just don't make a biopic. It's flat-out the laziest script I've ever seen, with every well-known anecdote about Cheney staged and shot without a hint of insight or perspective, and feeble or non-existent connective tissue between those scenes.

This is substance-free drivel that tries to convince stupid people that because it's about a significant historical figure it must be meaningful.

All that said, it's better-made than Rhapsody, and it is at least trying to pretend that it's about something important. I'm less upset about the Best Picture nod than about the nominations for Best Screenplay (It. Was. Horrible.), Best Director (although keeping the entire cast from acting at any point was impressive), and Best Actor (what a huge waste of Bale). I did think that Sam Rockwell stole the film, perhaps the only cast member who committed to some kind of angle on his character...but that's a low bar.


## Green Book

My main problem with this film wasn't the somewhat clichéd handling of historical racism, but rather the complete absence of subtext. Did you miss that look Frank gave the two black workers in his apartment? Or what he and his friends said about them? Or how he threw out the glasses they used to drink from? No worries: we'll have plenty of his friends just flat-out telling us that he's racist. Not sure what to make of Shirley acting uncomfortable socializing with the horseshoe-players at the motel? Not a problem; he'll explain it a few minutes later. They even used Frank's letters as a way of shoehorning in a redundant voiceover.

So even if this had something more modern to say about race and class than Driving Miss Daisy thirty years ago, and even if hadn't been revealed as a very lazy rewriting of history, it simply wasn't a good use of the medium. It was all tell, no show.


## Roma

This is the year's old-school classic filmmaking entry, and the first one that I think we shouldn't be embarrassed to see nominated.

I'm afraid this film's themes just didn't interest me, and without any real hook it all felt clichéd, from the glacial pace to the heavy-handed messaging to the use of black and white (if it were in color it wouldn't be "art"; it would just be boring). In short, nothing here felt new.

I found myself really hoping everyone would drown in the ocean.

I've got to compare this with 2016's Moonlight, which successfully took small-personal-journey-amidst-larger-social-issue in a direction I hadn't seen before. Roma didn't.

As much as I found the black and white pretentious, visually this film was absolutely gorgeous while still feeling richly authentic; of the nominees I've seen I guess I like Roma for Best Production Design and/or Best Cinematography.


## A Star Is Born

This is where I'd put my money for what the Academy will choose as its Best Picture: a classic story about show business for people in show business, which portrays show business as inherently righteous and meaningful. It shouldn't have worked as well as it did, but they pulled it off, thanks largely to a performance from Lady Gaga that I don't think anyone else could have delivered. (The Academy hasn't historically given Best Actress for portraying yourself, even if lightly fictionalized, however.) And the film managed its highest-degree-of-difficulty sequence in capturing the mediocrity of the SNL performance without hitting you over the head with it.

My first big complaint is that the male lead, in spite of a great performance from Cooper, was at least half lifted from a film I enjoyed more: 2009's Crazy Heart.

But more deeply, I actually found a lot of the film's message troubling. In the year of MeToo, this film expects us to just brush aside the power imbalance and sexual obligation in the first act? I don't understand how it's possible to watch this and not ask how the story would have turned if Ally had refused anything but a professional relationship. The film brushes up against questions of skill and artistry versus commercialization and image...but its actual engagement with those issues goes no farther than one character (not an altogether reliable guide to either industry success or life satisfaction) delivering a few bromides. Worst of all, the film's central dogma is that everybody has talent, but it's only the rare few with "something to say" who become stars—a transparently self-serving rationalization for why stars deserve their fame, and trivially refuted by our modern reality celebs who consistently demonstrate that being a star makes people think you have something to say, not vice versa.


## BlaKkKlansman

This is the film I had the most trouble ranking, and the first one we've come to that I don't think had any glaring problems: it was well-made, interesting, and had something to say. But it wasn't great at anything. It was fairly conventionally put together, the premise wasn't as edgy as it seemed to think it was, and what it had to say was...not that deep. It suffers for having come out in the same year as Sorry To Bother You (which I assume got made largely due to the success of Get Out, my favorite from last year), which was much more uneven but has stayed with me far longer than BlaKkKlansman.


## The Favourite

Now this is a classic Oscar nominee: amazing outfits, people doing accents, and a script whose main themes, and even plot points, are unspoken but still crystal clear.

That said, I was bored out of my mind for the first hour. (I only got interested when the chick fell off the horse and ended up with those gnarly scars.) The ending was an unsatisfying refutation of the prior two hours of Abigail's patience, resourcefulness, and initiative. Holding the shot for a pretentiously long time doesn't get them off the hook for that.

Also: nominating Emma Stone in the "supporting" category instead of Best Actress makes a mockery of the distinction. Shame on everyone.


## Black Panther

This was a *big* movie that managed to be both smart and entertaining. It managed this in spite of the constraints imposed by being part of the MCU, which is as complicated a cross-promotional strategy as it is a fictional universe. In a year with no truly memorable nominated films, my Best Picture vote would go to one that marries craft, commercialism, and commentary in a groundbreaking way.

What I most appreciated on my first viewing was that plot elements normally most in need of the audience's suspension of disbelief could be seen as contemporary political commentary: this nation chooses their leader not by merit but through ritual combat that could let a malicious outsider take over? (Apparently the overall message is that Hillary should find some uncounted votes and reverse the 2016 result.)

It did well enough with that commentary, as well as with its other political hot buttons, to be taken seriously...but if you do take it seriously and not as just a comic book movie there are a few issues:

1. There were plenty of strong female characters (both physically and mentally) throughout the film, but I don't think that makes up for the fact that every man but no woman has political agency. The dynamic of women as soldiers seems progressive, until they get passed from one king to another as servants. The constitutional absurdity of ritual combat also leaves a lot of issues in its wake. "Women can be great warriors!" is a maddeningly naïve response to the structural sexism maintained by contests of strength as prerequisites for political power.

2. The Wakandans go incognito to a high-end nightclub in Korea to break up the vibranium deal. It was a great sequence. But isn't the fact that black people have trouble seamlessly blending into high society almost anywhere in the world, like, a pretty salient part of the black experience? "Let's ignore race for a little while so we can focus on other goals" is, again, rather naïve treatment of one of the main issues this film is supposed to be so enlightened about. I understand why they did it—I don't have any thoughts on a better way to make that crucial sequence in the film work—but it's asking another major suspension of disbelief.

3. That ritual-combat-as-metaphor-for-political-campaigning thing I liked the first time through doesn't hold up so well on rewatching. Maybe tell me that of the five tribes, the Jabari (mountain) tribe never supports the king, the border tribe refuses to support T'Challa because of differences on foreign policy, and Killmonger's internal challenge within the ruling tribe robs T'Challa of the majority he'd need to avoid the absurd combat ritual—that's a fair starting point for a parable about the fragility of majoritarian democracy. But that's not in the film. Instead all the problems are caused by ritual combat among a tiny group of hereditary nobles with no semblance of democracy, and the film's "resolution" is merely that the more convenient dude won that ritual and he happened to come up with a new (but unclear) foreign policy for (unclear) reasons.

I also think this film is hurt by the release (almost a year later) of Spider-Man: Into the Spider-Verse, which I consider the first film to actually capture so many of the things that modern comic book fans love about comics. The MCU, including Black Panther, suddenly feels like it's made by and for people who haven't picked up a comic in thirty years.